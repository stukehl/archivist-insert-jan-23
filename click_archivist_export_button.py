#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to click export button
"""

import sys
import time

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By

driver = webdriver.Remote(
         command_executor='http://selenium-standalone-firefox:4444/wd/hub',
         desired_capabilities=DesiredCapabilities.FIREFOX)

# driver = webdriver.Firefox(executable_path="/home/jenny/Documents/python_scripts.git/geckodriver")


def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def click_export_button(uname, pw):
    """
    Click 'Export' for the only study
    """
    # log in
    archivist_url = "https://closer-build.herokuapp.com"
    export_url = "https://closer-build.herokuapp.com/admin/instruments/exports"
    archivist_login(archivist_url, uname, pw)
    driver.get(export_url)
    time.sleep(10)

    # locate id and link
    trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")
    tr = trs[0]

    # column 2 is "Prefix"
    xml_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
    print(xml_prefix)

    # column 4 is "Actions", click on "Create new export"
    #TODO: this is case sensitive
    print("Click 'CREATE NEW EXPORT' button for " + xml_prefix)
    exportButton = tr.find_element(By.LINK_TEXT, "CREATE NEW EXPORT").click()
    time.sleep(5)

    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    click_export_button(uname, pw)


if __name__ == "__main__":
    main()
